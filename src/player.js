//player.js

var Player = function(uuid, name){
  this.uuid = uuid;
  this.name = name;
  this.color = null; //String: include hash in the hex code
  this.number = 0;
  this.bigPieces = 3;
	this.midPieces = 3;
	this.pegPieces = 3;
};

Player.prototype.reset = function(){
		this.bigPieces = 3;
		this.midPieces = 3;
		this.pegPieces = 3;
		this.color = null;
		this.number = 0;
};

Player.prototype.hasPieces = function(piece){
	if (piece === 'b' && player.bigPieces === 0)
		return false;
	if (piece === 'm' && player.midPieces === 0)
		return false;
	if (piece === 'p' && player.pegPieces === 0)
		return false;
	return true;
};

Player.prototype.addGame = function(number, color){

};

module.exports = Player;
