
//Game is like a factory?
var Game = require('./game.js');
var Player = require('./player.js');
var express = require('express')();
var http = require('http').Server(express);
var io = require('socket.io')(http);

var id = 0;



//Hashing players by uid
var players = {};
var games = {};
players[123123123] = new Player(123123123, 'jeff');


var testGame = new Game();
testGame.id = "chuckles";

games[testGame.id] = testGame;


console.log(players[123123123]);

express.get('/', function(req, res){
  //Logic to check if they have localdata
  //if they have some, check if they are in a game
    //Bring them to game
  //else bring them to index
  res.sendFile(__dirname + '/client/index.html');
});

express.get('/:gameID', function(req, res){
  var gameID = req.params.gameID;

  if(gameID === 'makeNewGame'){
    //make a new game and send the player to it

    var newGame = new Game();
    id++;

    newGame.id = id; //Generate new gameID here;
    games[newGame.id] = newGame;
    console.log("new game: " + newGame.id);
    // res.append('gameID', id);
    res.redirect('/' + newGame.id);
    return;
  }

  var thisGame = games[gameID];
  if(thisGame !== null){

    res.sendFile(__dirname + '/client/game.html');
    //send them to the game
  }else{
    //That game does not exist
    console.log("Game DNE");
  }

});

// var initSocket = function(game){
//   game.socket = io('/' + game.id);

//   game.socket.
// }

io.on('connection', function(socket){

  socket.on('join', function(gameID){
    socket.join(gameID);
  });

	socket.on('register_player', function(uuid, gameID) {

    var player = players[uuid];
    var game = games[gameID];
    console.log("Registered " + uuid + " to game " + gameID);

		if(player === null){
      //Ask them to make a new name, make player, select color
      player = new Player(uuid, 'Elby');
      player.color = "yellow"; //DEBUG
      players[uuid] = player;
		}
		if(game.playerCount > 4){ //The set playercount
			console.log('a spectator connected.');
			return;
		}

		for(var i = 0; i < game.playerCount; i++){
		  if(game.players[i].uuid == player.uuid) //Can't add again
		    return;
		}
		player.number = game.playerCount;
		game.players[player.number] = player;
		game.playerCount++;
		io.to(gameID).emit('update_player', player);
	});

	socket.on('get_game', function(gameID){
	 // debugger;
		console.log(games[gameID]);
		io.to(gameID).emit('update_game', games[gameID]);
	});


	//use current game info to determine current player
	socket.on('send_move', function(move, gameID){
	  game = games[gameID];
	  player = game.getCurrentPlayer();

		if(player.hasPieces(move.charAt(2))){
			game.insertPiece(move, player);
			if(game.checkWin()){
			  game.winner = true;

			  //Add code to display info and let users disconnect
			}
		}else{
		// 	socket.emit('message', 'You have no more pieces of that type.');
		// 	console.log("!!NO MORE PIECES!!");
		}
	});
});

http.listen(3000, function(){
	console.log('listening on *:3000');
});
