//game.js
//Prototype for an Otrio game



//Maybe make a constructor that takes a player?
var Game = function(){

  //Is this necessary?
  function Square(){
	  this.big = "";
	  this.mid = "";
	  this.peg = "";
  }

  this.board =  [[new Square(), new Square(), new Square()],
			          [new Square(), new Square(), new Square()],
			          [new Square(), new Square(), new Square()]];

	this.players = [];

	this.id = 0;

	this.turn = 0;
	this.winner = false;
	this.playerCount = 0;
};

Game.prototype.getCurrentPlayer = function(){
  // console.log(this.players);
  // console.log(this.turn);
  return this.players[this.turn];
};

Game.prototype.insertPiece = function(spot, player){
	x = parseInt(spot.charAt(0), 10);
	y = parseInt(spot.charAt(1), 10);
	type = spot.charAt(2);
	game = this;
	destSquare = game.board[x][y];

	if(type == "b"){
		destSquare.big = player.uuid;
		player.bigPieces--;
	}else if(type == "m"){
		destSquare.mid = player.uuid;
		player.midPieces--;
	}else{
		destSquare.peg = player.uuid;
		player.pegPieces--;
	}

  console.log("tun" + game.turn);
  console.log(game.playerCount);
	game.turn++;
	if(game.turn > (game.playerCount - 1))
		game.turn = 0;

};

var winHelper = function(a, b, c){

	//ascending
	if((a.peg === b.mid) && (b.mid === c.big) && (b.mid !== ""))
		return true;

	//descending
	if((a.big === b.mid) && (b.mid === c.peg) && (b.mid !== ""))
		return true;

	//big
	if((a.big === b.big) && (b.big === c.big) && (b.big !== ""))
		return true;

	//mid
	if((a.mid === b.mid) && (b.mid === c.mid) && (b.mid !== ""))
		return true;

	//peg
	if((a.peg === b.peg) && (b.peg === c.peg) && (b.peg !== ""))
		return true;

	return false;
};

Game.prototype.checkWin = function(){
	//Each square
	board = this.board;
	for(i = 0; i < 3; i++){
		for(j = 0; j < 3; j++){
			square = board[i][j];
			if((square.big == square.mid) && (square.mid == square.peg) && (square.peg !== ""))
				return true;
		}
	}

	//horizontal
	for(i = 0; i < 3; i++){
		a = board[i][0];
		b = board[i][1];
		c = board[i][2];

		if(winHelper(a, b, c)) return true;
	}

	//Vertical
	for(i = 0; i < 3; i++){
		a = board[0][i];
		b = board[1][i];
		c = board[2][i];

		if(winHelper(a, b, c)) return true;
	}

	//Horizontal top to bottom
	a = board[0][0];
	b = board[1][1];
	c = board[2][2];

	if(winHelper(a, b, c)) return true;

	//Horizontal bottom to top
	a = board[0][2];
	c = board[2][0];
	if(winHelper(a, b, c)) return true;

};

module.exports = Game;
