
//Game is like a factory?
var Game = require('./game.js');
var Player = require('./player.js');
var express = require('express')();
var http = require('http').Server(express);
var io = require('socket.io')(http);

var id = 0;



//Hashing players by uid
var players = {};
var games = {};
players[123123123] = new Player(123123123, 'jeff');


var testGame = new Game();
testGame.id = "chuckles";

games[testGame.id] = testGame;


console.log(players[123123123]);

express.get('/', function(req, res){
  //Logic to check if they have localdata
  //if they have some, check if they are in a game
    //Bring them to game
  //else bring them to index
  res.sendFile(__dirname + '/client/index.html');
});

express.get('/:gameID', function(req, res){
  var gameID = req.params.gameID;

  if(gameID === 'makeNewGame'){
    //make a new game and send the player to it

    var newGame = new Game();
    id++;

    newGame.id = id; //Generate new gameID here;
    games[newGame.id] = newGame;
    console.log("new game: " + newGame.id);
    // res.append('gameID', id);
    res.redirect('/' + newGame.id);
    return;
  }

  var thisGame = games[gameID];
  if(thisGame !== null){

    res.sendFile(__dirname + '/client/game.html');
    //send them to the game
  }else{
    //That game does not exist
    console.log("Game DNE");
  }

});

// var initSocket = function(game){
//   game.socket = io('/' + game.id);

//   game.socket.
// }

io.on('connection', function(socket){

  socket.on('join', function(gameID){
    socket.join(gameID);
  });

	socket.on('register_player', function(uuid, gameID) {

    var player = players[uuid];
    var game = games[gameID];
    console.log("Registered " + uuid + " to game " + gameID);

		if(player === null){
      //Ask them to make a new name, make player, select color
      player = new Player(uuid, 'Elby');
      player.color = "yellow"; //DEBUG
      players[uuid] = player;
		}
		if(game.playerCount > 4){ //The set playercount
			console.log('a spectator connected.');
			return;
		}

		for(var i = 0; i < game.playerCount; i++){
		  if(game.players[i].uuid == player.uuid) //Can't add again
		    return;
		}
		player.number = game.playerCount;
		game.players[player.number] = player;
		game.playerCount++;
		io.to(gameID).emit('update_player', player);
	});

	socket.on('get_game', function(gameID){
	 // debugger;
		console.log(games[gameID]);
		io.to(gameID).emit('update_game', games[gameID]);
	});


	//use current game info to determine current player
	socket.on('send_move', function(move, gameID){
	  game = games[gameID];
	  player = game.getCurrentPlayer();

		if(player.hasPieces(move.charAt(2))){
			game.insertPiece(move, player);
			if(game.checkWin()){
			  game.winner = true;

			  //Add code to display info and let users disconnect
			}
		}else{
		// 	socket.emit('message', 'You have no more pieces of that type.');
		// 	console.log("!!NO MORE PIECES!!");
		}
	});
});

http.listen(3000, function(){
	console.log('listening on *:3000');
});
;//game.js
//Prototype for an Otrio game



//Maybe make a constructor that takes a player?
var Game = function(){

  //Is this necessary?
  function Square(){
	  this.big = "";
	  this.mid = "";
	  this.peg = "";
  }

  this.board =  [[new Square(), new Square(), new Square()],
			          [new Square(), new Square(), new Square()],
			          [new Square(), new Square(), new Square()]];

	this.players = [];

	this.id = 0;

	this.turn = 0;
	this.winner = false;
	this.playerCount = 0;
};

Game.prototype.getCurrentPlayer = function(){
  // console.log(this.players);
  // console.log(this.turn);
  return this.players[this.turn];
};

Game.prototype.insertPiece = function(spot, player){
	x = parseInt(spot.charAt(0), 10);
	y = parseInt(spot.charAt(1), 10);
	type = spot.charAt(2);
	game = this;
	destSquare = game.board[x][y];

	if(type == "b"){
		destSquare.big = player.uuid;
		player.bigPieces--;
	}else if(type == "m"){
		destSquare.mid = player.uuid;
		player.midPieces--;
	}else{
		destSquare.peg = player.uuid;
		player.pegPieces--;
	}

  console.log("tun" + game.turn);
  console.log(game.playerCount);
	game.turn++;
	if(game.turn > (game.playerCount - 1))
		game.turn = 0;

};

var winHelper = function(a, b, c){

	//ascending
	if((a.peg === b.mid) && (b.mid === c.big) && (b.mid !== ""))
		return true;

	//descending
	if((a.big === b.mid) && (b.mid === c.peg) && (b.mid !== ""))
		return true;

	//big
	if((a.big === b.big) && (b.big === c.big) && (b.big !== ""))
		return true;

	//mid
	if((a.mid === b.mid) && (b.mid === c.mid) && (b.mid !== ""))
		return true;

	//peg
	if((a.peg === b.peg) && (b.peg === c.peg) && (b.peg !== ""))
		return true;

	return false;
};

Game.prototype.checkWin = function(){
	//Each square
	board = this.board;
	for(i = 0; i < 3; i++){
		for(j = 0; j < 3; j++){
			square = board[i][j];
			if((square.big == square.mid) && (square.mid == square.peg) && (square.peg !== ""))
				return true;
		}
	}

	//horizontal
	for(i = 0; i < 3; i++){
		a = board[i][0];
		b = board[i][1];
		c = board[i][2];

		if(winHelper(a, b, c)) return true;
	}

	//Vertical
	for(i = 0; i < 3; i++){
		a = board[0][i];
		b = board[1][i];
		c = board[2][i];

		if(winHelper(a, b, c)) return true;
	}

	//Horizontal top to bottom
	a = board[0][0];
	b = board[1][1];
	c = board[2][2];

	if(winHelper(a, b, c)) return true;

	//Horizontal bottom to top
	a = board[0][2];
	c = board[2][0];
	if(winHelper(a, b, c)) return true;

};

module.exports = Game;
;//player.js

var Player = function(uuid, name){
  this.uuid = uuid;
  this.name = name;
  this.color = null; //String: include hash in the hex code
  this.number = 0;
  this.bigPieces = 3;
	this.midPieces = 3;
	this.pegPieces = 3;
};

Player.prototype.reset = function(){
		this.bigPieces = 3;
		this.midPieces = 3;
		this.pegPieces = 3;
		this.color = null;
		this.number = 0;
};

Player.prototype.hasPieces = function(piece){
	if (piece === 'b' && player.bigPieces === 0)
		return false;
	if (piece === 'm' && player.midPieces === 0)
		return false;
	if (piece === 'p' && player.pegPieces === 0)
		return false;
	return true;
};

Player.prototype.addGame = function(number, color){

};

module.exports = Player;
